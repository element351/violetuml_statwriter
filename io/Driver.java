package io;

public class Driver {

	public static void main(String[] args) {
		FileManager fm = new FileManager();

		fm.readFile("example_file.txt");
		System.out.println(fm.getNumObjects());
		for (int i = 0; i < fm.getNumObjects(); i++) {
			System.out.println(fm.getNameArr()[i] + ":" + fm.getOutMsgArr()[i] + "," + fm.getRtrnMsgArr()[i]);
		}
		fm.writeFile("output_from_exampleFile.txt");
	}

}
