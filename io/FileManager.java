package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileManager {
	private String[] nameArr;
	private int[] outMsgArr;
	private int[] rtrnMsgArr;
	private int numObjects;

	/**
	 * Default Constructor
	 */
	FileManager() {

	}

	// Used to create int array
	static int[] toIntArray(List<Integer> list) {
		int[] ret = new int[list.size()];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = list.get(i);
		}
		return ret;
	}

	/**
	 * Reads a .txt file containing statistical information takes the data and
	 * fills the FileManager objects's attributes with the data
	 * 
	 * @param inputFile
	 *            string containing file path
	 */
	public void readFile(String inputFile) {
		BufferedReader bReader = null;

		// temporary ArrayLists: these will at some point be used to fill the
		// objects various arrays
		List<String> objList = new ArrayList<String>();
		List<Integer> numOutMsg = new ArrayList<Integer>();
		List<Integer> numRtrnMsg = new ArrayList<Integer>();

		try {
			// .txt File from Class model Diagram containing information
			bReader = new BufferedReader(new FileReader(inputFile));
			String str;

			// read lines and put information into the 3 different Lists
			while ((str = bReader.readLine()) != null) {
				String name = str.substring(0, str.indexOf(":"));
				objList.add(name);

				String out = str.substring(str.indexOf(":") + 1, str.indexOf(","));
				numOutMsg.add(Integer.parseInt(out));

				String rec = str.substring(str.indexOf(",") + 1);
				numRtrnMsg.add(Integer.parseInt(rec));
			}

			// Convert lists to arrays a writes to class attributes
			nameArr = objList.toArray(new String[0]);
			outMsgArr = toIntArray(numOutMsg);
			rtrnMsgArr = toIntArray(numRtrnMsg);
			numObjects = nameArr.length;
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void writeFile(String outputFile) {
		BufferedWriter bWriter = null;

		try {
			bWriter = new BufferedWriter(new FileWriter(outputFile));
			for (int i = 0; i < numObjects; i++) {
				String str = nameArr[i] + ":" + outMsgArr[i] + "," + rtrnMsgArr[i] + "\n";
				bWriter.write(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the nameArr
	 */
	public String[] getNameArr() {
		return nameArr;
	}

	/**
	 * @return the outMsgArr
	 */
	public int[] getOutMsgArr() {
		return outMsgArr;
	}

	/**
	 * @return the rtrnMsgArr
	 */
	public int[] getRtrnMsgArr() {
		return rtrnMsgArr;
	}

	/**
	 * @return the numObjects
	 */
	public int getNumObjects() {
		return numObjects;
	}

	/**
	 * @param nameArr
	 *            the nameArr to set
	 */
	public void setNameArr(String[] nameArr) {
		this.nameArr = nameArr;
	}

	/**
	 * @param outMsgArr
	 *            the outMsgArr to set
	 */
	public void setOutMsgArr(int[] outMsgArr) {
		this.outMsgArr = outMsgArr;
	}

	/**
	 * @param rtrnMsgArr
	 *            the rtrnMsgArr to set
	 */
	public void setRtrnMsgArr(int[] rtrnMsgArr) {
		this.rtrnMsgArr = rtrnMsgArr;
	}

	/**
	 * @param numObjects
	 *            the numObjects to set
	 */
	public void setNumObjects(int numObjects) {
		this.numObjects = numObjects;
	}

}
